<?php

namespace Drupal\commerce_eta_abandoned_carts\EventSubscriber;

use Drupal\commerce_eta\Event\Event;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\views\ViewEntityInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Subscribes to Abandoned Carts Events.
 */
class EmailSubscriber implements EventSubscriberInterface {

  /**
   * Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Abandoned Carts Configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Drupal Mail Plugin Manager.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * Constructs the email event subscriber.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity Type Manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Drupal Configuration Factory.
   * @param \Drupal\Core\Mail\MailManagerInterface $mail_manager
   *   Drupal Mail Plugin Manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory, MailManagerInterface $mail_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->config = $config_factory->get('commerce_eta_abandoned_carts.abandoned_carts');
    $this->mailManager = $mail_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      'commerce_eta.abandoned_carts' => 'sendEmail',
    ];
  }

  /**
   * This method is called when the commerce_eta.abandoned_carts is dispatched.
   *
   * @param \Drupal\commerce_eta\Event\Event $event
   *   The dispatched event.
   */
  public function sendEmail(Event $event) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $event->getEntity();
    $module = 'commerce_eta_abandoned_carts';
    $key = 'abandoned_cart_mail';
    $to = $order->getEmail();
    $params['order_view'] = $this->buildOrderTable($event);
    $langcode = $order->getCustomer()->getPreferredLangcode(TRUE);
    $this->mailManager->mail($module, $key, $to, $langcode, $params, NULL, TRUE);
  }

  /**
   * Renders the view chosen in the form or the default.
   *
   * @param \Drupal\commerce_eta\Event\Event $event
   *   OrderEvent definition.
   *
   * @return array
   *   Render array to place in the email.
   */
  private function buildOrderTable(Event $event) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $event->getEntity();

    if ($view_id = $this->config->get('order_view')) {
      $view = $this->entityTypeManager->getStorage('view')->load($view_id);
    }
    else {
      $view = $this->entityTypeManager->getStorage('view')->load('abandoned_cart_item_table');
    }

    if ($view instanceof ViewEntityInterface) {
      $executable = $view->getExecutable();
      $executable->setArguments([$order->id()]);
      // @todo Set this in the config form.
      $executable->setDisplay('email');
      $executable->preExecute();
      $executable->execute();
      return $executable->render('master');
    }
  }

}
