<?php

namespace Drupal\commerce_eta\Form;

use Drupal\commerce\ConditionManagerInterface;
use Drupal\commerce_eta\Entity\TriggerInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form to create and edit Trigger config entities.
 */
class TriggerForm extends EntityForm {

  /**
   * Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Commerce Condition Plugin Manager.
   *
   * @var \Drupal\commerce\ConditionManagerInterface
   */
  protected $conditionManager;

  /**
   * Constructs the trigger form.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity Type Manager.
   * @param \Drupal\commerce\ConditionManagerInterface $condition_manager
   *   Commerce Condition Plugin Manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ConditionManagerInterface $condition_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->conditionManager = $condition_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) : static {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_condition'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\commerce_eta\Entity\Trigger $trigger */
    $trigger = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $trigger->label(),
      '#description' => $this->t("Label for the Trigger."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $trigger->id(),
      '#machine_name' => [
        'exists' => '\Drupal\commerce_eta\Entity\Trigger::load',
      ],
      '#disabled' => !$trigger->isNew(),
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Trigger'),
      '#description' => $this->t('Trigger will not fire events if disabled, although you can still see the next-run results on the triggers overview page'),
      '#default_value' => !($trigger->isNew()) && $trigger->getStatus(),
    ];

    $form['cron_status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Fire on cron run.'),
      '#default_value' => $trigger->isNew() ? NULL : $trigger->getCronStatus(),
      '#description' => $this->t('When checked, this trigger will fire on each cron run. Depending on the size of your site (amount of entities, conditions, run limits, etc), this may have a large performance impact on your web workers during cron. For large sites, use the Drupal Console commands to pull triggers.'),
    ];

    $form['log_status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log events.'),
      '#default_value' => $trigger->isNew() ? NULL : $trigger->getLogStatus(),
      '#description' => $this->t('When checked, all events fired by this trigger will be logged.'),
    ];

    $form['run_limit'] = [
      '#type' => 'number',
      '#title' => $this->t('Limit'),
      '#description' => $this->t('The maximum times this trigger can fire an event on the same entity. Leave blank to disable the limiter.'),
      '#default_value' => $trigger->isNew() ? NULL : $trigger->getRunLimit(),
    ];

    $has_events = $this->hasEvents($trigger);

    $form['target_entity_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Commerce Entity Type'),
      '#default_value' => $trigger->isNew() ? NULL : $trigger->getTargetEntityTypeId(),
      '#disabled' => $has_events,
      '#description' => $has_events ? $this->t("This trigger has already fired events. Please purge the events for this trigger to change the base entity type.") : $this->t("Select an entity type"),
      '#size' => 1,
      '#required' => TRUE,
      '#options' => $this->getEntityTypeOptions(),
      '#ajax' => [
        'callback' => '::getEntityTypeConditionsForm',
        'wrapper' => 'entity-type-conditions',
        'event' => 'change',
      ],
    ];

    $form['entity_type_conditions'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'entity-type-conditions',
      ],
    ];

    if ($trigger->isNew()) {
      $entity_type = $form_state->getValue('target_entity_type');
    }
    else {
      $entity_type = $trigger->getTargetEntityTypeId();
    }

    if (!empty($entity_type)) {
      $form['entity_type_conditions']['conditions'] = [
        '#type' => 'commerce_conditions',
        '#title' => $this->t('Conditions'),
        '#parent_entity_type' => 'commerce_event_trigger',
        '#entity_types' => [$entity_type],
        '#default_value' => $trigger->get('conditions'),
      ];

      $form['entity_type_conditions']['conditionOperator'] = [
        '#type' => 'radios',
        '#title' => $this->t('Condition operator'),
        '#title_display' => 'invisible',
        '#options' => [
          'AND' => $this->t('All conditions must pass'),
          'OR' => $this->t('Only one condition must pass'),
        ],
        '#default_value' => $trigger->getConditionOperator(),
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $trigger = $this->entity;
    $status = $trigger->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Trigger.', [
          '%label' => $trigger->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Trigger.', [
          '%label' => $trigger->label(),
        ]));
    }
    $form_state->setRedirectUrl($trigger->toUrl('collection'));
  }

  /**
   * Determines if a trigger has fired events.
   *
   * @param \Drupal\commerce_eta\Entity\TriggerInterface $trigger
   *   The trigger entity.
   *
   * @return bool
   *   Returns TRUE if trigger has fired events previously, FALSE otherwise.
   */
  private function hasEvents(TriggerInterface $trigger) : bool {
    return (bool) $this->entityTypeManager->getStorage('commerce_event_log_item')->getQuery()
      ->accessCheck(FALSE)
      ->condition('trigger', $trigger->id())
      ->count()
      ->execute();
  }

  /**
   * Gets the conditions form for the selected entity type.
   *
   * @param array $form
   *   This form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   This forms state.
   *
   * @return array
   *   Returns the form array for the entity type conditions.
   */
  public function getEntityTypeConditionsForm(array $form, FormStateInterface $form_state) {
    return $form['entity_type_conditions'];
  }

  /**
   * Gets the entity types that triggers can run on.
   *
   * @return string[]
   *   The entity type ids.
   */
  private function getEntityTypeOptions() {
    $entity_types = [];

    foreach ($this->conditionManager->getDefinitions() as $plugin_definition) {
      if (!array_key_exists($plugin_definition['entity_type'], $entity_types)) {
        // Only allow entity types that are installed.
        if ($this->entityTypeManager->hasDefinition($plugin_definition['entity_type'])) {
          $entity_types[$plugin_definition['entity_type']] = $this->entityTypeManager->getDefinition($plugin_definition['entity_type'])->getLabel();
        }
      }
    }

    return $entity_types;
  }

}
