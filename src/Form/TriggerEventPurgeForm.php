<?php

namespace Drupal\commerce_eta\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Builds the form to delete Trigger entities.
 */
class TriggerEventPurgeForm extends EntityConfirmFormBase {

  /**
   * Event Log Item Storage.
   *
   * @var \Drupal\commerce_eta\EventLogItemStorage
   */
  protected $logItemStorage;

  /**
   * Constructs the trigger form.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity Type Manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->logItemStorage = $entity_type_manager->getStorage('commerce_event_log_item');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) : static {
    return new static(
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_eta\Entity\TriggerInterface $trigger */
    $trigger = $this->entity;

    $form['purge_config'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Delete @count events.', [
        '@count' => $this->logItemStorage->getCount($trigger),
      ]),
      '#default_value' => FALSE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    /** @var \Drupal\commerce_eta\Entity\TriggerInterface $trigger */
    $trigger = $this->entity;
    return $this->t('Purge @count events fired by @label?', [
      '@count' => $this->logItemStorage->getCount($trigger),
      '@label' => $this->entity->label(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.commerce_event_trigger.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Purge Events');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_eta\Entity\TriggerInterface $trigger */
    $trigger = $this->entity;

    $count = 0;
    /** @var \Drupal\commerce_eta\Entity\Trigger $this->entity */
    // Remove all events for this trigger.
    if ($form_state->getValue('purge_config')) {
      $count = $this->logItemStorage->getCount($trigger);
      $this->logItemStorage->purge($trigger);
    }

    $this->messenger()->addMessage(
      $this->t("Purged @count events on @label", [
        '@count' => $count,
        '@label' => $trigger->label(),
      ])
    );

    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
