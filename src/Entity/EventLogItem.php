<?php

namespace Drupal\commerce_eta\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the event log item entity class.
 *
 * @ContentEntityType(
 *   id = "commerce_event_log_item",
 *   label = @Translation("Event Log Item"),
 *   label_collection = @Translation("Event Log Items"),
 *   bundle_label = @Translation("Trigger"),
 *   handlers = {
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\event_log_item\EventLogItemAccessControlHandler",
 *     "storage" = "Drupal\commerce_eta\EventLogItemStorage",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "commerce_event_log_item",
 *   admin_permission = "administer event log items",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "trigger",
 *     "label" = "id",
 *     "uuid" = "uuid"
 *   },
 *   bundle_entity_type = "commerce_event_trigger",
 * )
 */
class EventLogItem extends ContentEntityBase implements EventLogItemInterface {

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage, array &$values) {
    parent::preCreate($storage, $values);

    // Set the event timestamp to now.
    if (!isset($values['timestamp'])) {
      $values['timestamp'] = \Drupal::time()->getRequestTime();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetEntity() : ?ContentEntityInterface {
    if (!$this->get('entity')->isEmpty()) {
      /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $list */
      $list = $this->get('entity');
      /** @var \Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem $first */
      $first = $list->first();
      /** @var \Drupal\Core\Entity\Plugin\DataType\EntityReference $reference */
      $reference = $first->get('entity');
      $target = $reference->getTarget();
      if ($target) {
        return $target->getValue();
      }
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getTimestamp(): int {
    return $this->get('timestamp')->first()->get('value')->getCastedValue();
  }

  /**
   * {@inheritdoc}
   */
  public function getTrigger(): TriggerInterface {
    return Trigger::load($this->get('trigger')->first()->getString());
  }

  /**
   * {@inheritdoc}
   */
  public function getTriggerId(): string {
    return $this->bundle();
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['timestamp'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Event Time'))
      ->setDescription(t('The time that the event was fired.'))
      ->setCardinality(1)
      ->setRequired(TRUE)
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['entity'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Entity'))
      ->setDescription(t('The entity the event was fired on.'))
      ->setCardinality(1)
      ->setRequired(TRUE)
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public static function bundleFieldDefinitions(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions) {
    /** @var \Drupal\commerce_eta\Entity\TriggerInterface $bundle */
    $bundle = \Drupal::entityTypeManager()->getStorage('commerce_event_trigger')->load($bundle);

    // Get the parent bundle fields.
    $fields = parent::bundleFieldDefinitions($entity_type, $bundle, $base_field_definitions);

    // Clone the entity reference field and set the target type.
    $fields['entity'] = clone $base_field_definitions['entity'];
    $target_entity_id = $bundle->getTargetEntityTypeId();
    if ($target_entity_id) {
      // Set the reference target type on the entity.
      $fields['entity']->setSetting('target_type', $bundle->getTargetEntityTypeId());
    }

    return $fields;
  }

}
