<?php

namespace Drupal\commerce_eta;

use Drupal\commerce_eta\Entity\TriggerInterface;
use Drupal\commerce_eta\Event\Event;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Dispatches events from triggers.
 */
class TriggerDispatch {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Event Log Item Storage.
   *
   * @var \Drupal\Core\Entity\Sql\SqlEntityStorageInterface
   */
  protected $eventLogStorage;

  /**
   * Time Service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Event Dispatch Service.
   *
   * @var \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher
   */
  protected $eventDispatcher;

  /**
   * The logger.channel.commerce_eta service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $log;

  /**
   * Placeholder for caching prior event data.
   *
   * @var array
   */
  protected $eventCache = [];

  /**
   * Placeholder for caching event run counts.
   *
   * @var array
   */
  protected $runCountCache = [];

  /**
   * Builds up the dispatch service.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger_channel
   *   The logger.channel.commerce_eta service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   Drupal Time service.
   * @param \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher $event_dispatcher
   *   Event Dispatch Service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, LoggerChannelInterface $logger_channel, TimeInterface $time, ContainerAwareEventDispatcher $event_dispatcher) {
    $this->entityTypeManager = $entity_type_manager;
    $this->eventLogStorage = $entity_type_manager->getStorage('commerce_event_log_item');
    $this->time = $time;
    $this->eventDispatcher = $event_dispatcher;
    $this->log = $logger_channel;
  }

  /**
   * Fires events for an event trigger.
   *
   * @param \Drupal\commerce_eta\Entity\TriggerInterface $trigger
   *   The trigger to fire events against.
   * @param bool $dry
   *   Set to TRUE to dry fire (events won't dispatch), FALSE to fire events.
   *
   * @return int
   *   Returns the number of events fired against the trigger.
   */
  public function fire(TriggerInterface $trigger, bool $dry = FALSE) : int {
    if (!$trigger->getStatus()) {
      $this->log->notice($this->t("Trigger @label is disabled and will not fire. Attempt blocked.", [
        '@label' => $trigger->label(),
      ]));
      return 0;
    }
    // Load the entity storage.
    $entity_storage = $this->entityTypeManager->getStorage($trigger->getTargetEntityTypeId());

    $entity_filter_ids = $this->getEntityIdFilter($trigger);

    // Get the entity id key for reference lookups.
    $id_key = $this->entityTypeManager->getDefinition($trigger->getTargetEntityTypeId())->getKey('id');

    // Query for all entities within the type.
    $entity_query = $entity_storage->getQuery()->accessCheck(FALSE);
    // Sorting this way *probably* gets events moving quicker out of the gate.
    $entity_query->sort($id_key, 'DESC');
    // Add the id filter to the query if needed.
    if ($entity_filter_ids) {
      $entity_query->condition($id_key, $entity_filter_ids, "NOT IN");
    }
    // Execute the query.
    $entity_ids = $entity_query->execute();

    $event_count = 0;

    // Leave early if there's no entities to work on.
    if (!$entity_ids) {
      return $event_count;
    }

    // Initialize the run count cache.
    $this->initializeRunCounts($trigger);

    // Split the entity list into chunks to keep memory usage down.
    $chunks = array_chunk($entity_ids, 100);
    // Load entities in chunks to reduce overall memory footprint.
    foreach ($chunks as $chunk) {
      // @todo integrate with Commerce Entity Filter to reduce execution load.
      $entities = $this->loadEntities($trigger, $chunk);
      foreach ($entities as $entity) {
        if ($trigger->applies($entity)) {
          // Fire event.
          $this->fireOnEntity($trigger, $entity, $dry);
        }
      }

      // Reset the cache in entity storage for the chunk.
      // This helps to keep memory leaks away.
      $entity_storage->resetCache($chunk);

      // Remove the entities from memory completely.
      unset($entities);
    }

    return $event_count;
  }

  /**
   * Fires an event on a single entity.
   *
   * @param \Drupal\commerce_eta\Entity\TriggerInterface $trigger
   *   The trigger that is firing the event.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to fire the event against.
   * @param bool $dry
   *   Set to TRUE if event should not actually fire.
   *
   * @return int
   *   Returns the new run count on the entity.
   */
  public function fireOnEntity(TriggerInterface $trigger, EntityInterface $entity, bool $dry = FALSE) : int {
    // Triggers should never fire on mismatched entities.
    assert($trigger->getTargetEntityTypeId() === $entity->getEntityTypeId());

    /** @var \Drupal\commerce_eta\Entity\EventLogItemInterface $log_item */
    $log_item = $this->eventLogStorage->create([
      'trigger' => $trigger->id(),
      'entity' => $entity->id(),
      'timestamp' => $this->time->getRequestTime(),
    ]);
    if (!$dry) {
      $log_item->save();
    }

    // Get the new run count for the entity.
    $count = $this->getUpdatedRunCount($trigger, $entity);
    // Create the event class.
    $event = new Event($trigger, $entity, $log_item, $count);
    // Fire the event.
    if (!$dry) {
      $event = $this->eventDispatcher->dispatch($event, $trigger->getEventName());
    }
    // Log the event if enabled by the trigger config.
    if ($trigger->getLogStatus()) {
      $this->logEvent($event, $dry);
    }

    return $count;
  }

  /**
   * Gets the new count for an entity upon firing an event.
   *
   * @param \Drupal\commerce_eta\Entity\TriggerInterface $trigger
   *   The trigger.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return int
   *   Returns a new run count value.
   */
  public function getUpdatedRunCount(TriggerInterface $trigger, EntityInterface $entity) : int {
    $current_count = 0;

    $initialized = isset($this->runCountCache[$trigger->getTargetEntityTypeId()]);

    if (!$initialized) {
      $this->initializeRunCounts($trigger);
    }

    if (isset($this->runCountCache[$trigger->getTargetEntityTypeId()][$entity->id()])) {
      $current_count = $this->runCountCache[$trigger->getTargetEntityTypeId()][$entity->id()];
    }

    return $current_count + 1;
  }

  /**
   * Loads entities for firing events on a trigger.
   *
   * @param \Drupal\commerce_eta\Entity\TriggerInterface $trigger
   *   The trigger.
   * @param array $ids
   *   An array of entity ids to load.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   Returns an array of entities to fire events on.
   */
  public function loadEntities(TriggerInterface $trigger, array $ids) {
    $storage = $this->entityTypeManager->getStorage($trigger->getTargetEntityTypeId());
    $entities = [];
    switch ($storage->getEntityTypeId()) {
      // Edge case for order entities so they don't refresh on load.
      case "commerce_order":
        foreach ($ids as $id) {
          $entities[] = $storage->loadUnchanged($id);
        }
        break;

      // All other entities should just load normal.
      default:
        $entities = $storage->loadMultiple($ids);
    }

    return $entities;
  }

  /**
   * Generates a run count for all events fired by a trigger.
   *
   * @param \Drupal\commerce_eta\Entity\TriggerInterface $trigger
   *   The trigger.
   *
   * @return array
   *   Returns an aggregated array of entities and their run counts.
   */
  public function getEvents(TriggerInterface $trigger) {
    if (isset($this->eventCache[$trigger->getTargetEntityTypeId()]) && $this->eventCache[$trigger->getTargetEntityTypeId()]) {
      return $this->eventCache[$trigger->getTargetEntityTypeId()];
    }
    // Get the current run counts for all entities.
    $event_query = $this->eventLogStorage->getAggregateQuery();
    $event_query->accessCheck(FALSE);
    $event_query->condition('trigger', $trigger->id());
    $event_query->conditionAggregate('entity', 'COUNT', $trigger->getRunLimit(), ">=");
    $event_query->groupBy('entity');

    return $this->eventCache[$trigger->getTargetEntityTypeId()] = $event_query->execute();
  }

  /**
   * Generates and run count array for entities.
   *
   * @param \Drupal\commerce_eta\Entity\TriggerInterface $trigger
   *   The trigger to generate the count for.
   *
   * @return array
   *   Returns a key / value array containing entity => run count.
   */
  public function initializeRunCounts(TriggerInterface $trigger) : array {
    if (isset($this->runCountCache[$trigger->getTargetEntityTypeId()]) && $this->runCountCache[$trigger->getTargetEntityTypeId()]) {
      return $this->runCountCache[$trigger->getTargetEntityTypeId()];
    }

    $run_counts = [];
    foreach ($this->getEvents($trigger) as $result) {
      // Add the run count to the lookup array.
      $run_counts[$result['entity']] = (int) $result['entity_count'];
    }

    return $this->runCountCache[$trigger->getTargetEntityTypeId()] = $run_counts;
  }

  /**
   * Generates a list of entity ids to filter by for the trigger.
   *
   * @param \Drupal\commerce_eta\Entity\TriggerInterface $trigger
   *   The trigger entity.
   *
   * @return array
   *   Returns an array of entity ids to use as a query filter.
   */
  public function getEntityIdFilter(TriggerInterface $trigger) {
    $entity_filter_ids = [];
    foreach ($this->getEvents($trigger) as $result) {
      // Check the run count limit and add to the query filter.
      if ($result['entity_count'] >= $trigger->getRunLimit()) {
        $entity_filter_ids[] = $result['entity'];
      }
    }

    return $entity_filter_ids;
  }

  /**
   * Logs the event.
   *
   * @param \Drupal\commerce_eta\Event\Event $event
   *   The event to log.
   * @param bool $dry
   *   Determines whether the log should explain dry firing or not.
   */
  public function logEvent(Event $event, bool $dry) {
    $this->log->info($event->getTrigger()->label() . ($dry ? " dry" : "") . " fired event on " . $event->getEntity()->label());
  }

}
