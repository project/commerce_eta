<?php

namespace Drupal\commerce_eta\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides an interface for defining Trigger entities.
 */
interface TriggerInterface extends ConfigEntityInterface {

  /**
   * Gets the event name to subscribe to.
   *
   * @return string
   *   Returns the event name.
   */
  public function getEventName() : string;

  /**
   * Gets the enabled status of the Trigger entity.
   *
   * @return bool
   *   Returns TRUE if trigger is enabled, FALSE otherwise.
   */
  public function getStatus() : bool;

  /**
   * The Commerce entity type id for this trigger.
   *
   * @return string
   *   Returns the entity type id as a string.
   */
  public function getTargetEntityTypeId() : string;

  /**
   * Gets the status of logging for the trigger.
   *
   * @return bool
   *   Returns TRUE if logging is enabled, FALSE otherwise.
   */
  public function getLogStatus() : bool;

  /**
   * Gets the limit of how many times this trigger can run on any one entity.
   *
   * @return int|null
   *   The limit number, NULL if disabled.
   */
  public function getRunLimit() : ?int;

  /**
   * Gets the status of cron for this trigger.
   *
   * @return bool
   *   Returns TRUE if cron is enabled for this trigger.
   */
  public function getCronStatus() : bool;

  /**
   * Gets the Commerce Conditions of the Trigger.
   *
   * @return \Drupal\commerce\Plugin\Commerce\Condition\ConditionInterface[]
   *   The conditions array of the Trigger.
   */
  public function getConditions() : array;

  /**
   * Gets the Trigger condition operator.
   *
   * @return string
   *   The condition operator. Possible values: AND, OR.
   */
  public function getConditionOperator() : string;

  /**
   * Checks whether the trigger applies to the given entity.
   *
   * Ensures that the conditions pass.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return bool
   *   TRUE if entity applies, FALSE otherwise.
   */
  public function applies(EntityInterface $entity) : bool;

}
