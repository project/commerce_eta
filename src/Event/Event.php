<?php

namespace Drupal\commerce_eta\Event;

use Drupal\commerce_eta\Entity\EventLogItemInterface;
use Drupal\commerce_eta\Entity\Trigger;
use Drupal\commerce_eta\Entity\TriggerInterface;
use Drupal\Component\EventDispatcher\Event as CoreEvent;
use Drupal\Core\Entity\EntityInterface;

/**
 * Custom event class to handle ETA events.
 */
class Event extends CoreEvent {

  /**
   * The Trigger entity that is firing this event.
   *
   * @var \Drupal\commerce_eta\Entity\Trigger
   */
  protected $trigger;

  /**
   * The entity this event is firing on.
   *
   * @var \Drupal\Core\Entity\ContentEntityInterface
   */
  protected $entity;

  /**
   * The event log item entity.
   *
   * @var \Drupal\commerce_eta\Entity\EventLogItemInterface
   */
  protected $eventLogItem;

  /**
   * The number of times the trigger has fired on this entity.
   *
   * @var int
   */
  protected $runCount;

  /**
   * Generates an OrderEvent object to fire with the dispatcher.
   *
   * @param \Drupal\commerce_eta\Entity\Trigger $trigger
   *   The Trigger entity that is firing this event.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity this event is firing on.
   * @param \Drupal\commerce_eta\Entity\EventLogItemInterface $event_log_item
   *   The event log item entity.
   * @param int $run_count
   *   Number of times trigger has fired on this entity, INCLUDING this one.
   */
  public function __construct(Trigger $trigger, EntityInterface $entity, EventLogItemInterface $event_log_item, int $run_count) {
    $this->trigger = $trigger;
    $this->entity = $entity;
    $this->eventLogItem = $event_log_item;
    $this->runCount = $run_count;
  }

  /**
   * Gets the entity the event is firing on.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Returns the entity.
   */
  public function getEntity() : EntityInterface {
    return $this->entity;
  }

  /**
   * Helper function to get the Trigger entity that fired this event.
   *
   * @return \Drupal\commerce_eta\Entity\Trigger
   *   Event Trigger entity definition.
   */
  public function getTrigger() : TriggerInterface {
    return $this->trigger;
  }

  /**
   * Gets the event log item entity for the event.
   *
   * @return \Drupal\commerce_eta\Entity\EventLogItemInterface
   *   Returns the entity.
   */
  public function getEventLogItem() : EventLogItemInterface {
    return $this->eventLogItem;
  }

  /**
   * Helper function to get the number of times this event has fired previously.
   *
   * @return int
   *   The number or null if event has never fired on the entity.
   */
  public function getRunCount() : int {
    return $this->runCount;
  }

}
