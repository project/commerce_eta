<?php

namespace Drupal\commerce_eta;

use Drupal\commerce_eta\Entity\TriggerInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;

/**
 * Custom storage class for event log items.
 */
class EventLogItemStorage extends SqlContentEntityStorage {

  /**
   * Gets an event count for a given trigger.
   *
   * @param \Drupal\commerce_eta\Entity\TriggerInterface $trigger
   *   The trigger.
   *
   * @return int
   *   The number of events fired by the trigger.
   */
  public function getCount(TriggerInterface $trigger) : int {
    return $this->getQuery()->accessCheck(FALSE)
      ->condition('trigger', $trigger->id())
      ->count()
      ->execute();
  }

  /**
   * Purges all events for a given trigger.
   *
   * @param \Drupal\commerce_eta\Entity\TriggerInterface $trigger
   *   The trigger.
   */
  public function purge(TriggerInterface $trigger) {
    $ids = $this->getQuery()
      ->accessCheck(FALSE)
      ->condition('trigger', $trigger->id())
      ->execute();

    if (count($ids) > 200) {
      $chunks = array_chunk($ids, 200);
      foreach ($chunks as $chunk) {
        $this->delete($this->loadMultiple($chunk));
      }
    }
    else {
      $this->delete($this->loadMultiple($ids));
    }
  }

}
