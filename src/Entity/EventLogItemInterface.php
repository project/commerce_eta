<?php

namespace Drupal\commerce_eta\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * Interface for event log item entities.
 */
interface EventLogItemInterface extends ContentEntityInterface {

  /**
   * Gets the timestamp that the event was fired.
   *
   * @return int
   *   Unix timestamp.
   */
  public function getTimestamp() : int;

  /**
   * Gets the target entity of the event.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The entity, or NULL if the entity no longer exists.
   */
  public function getTargetEntity() : ?EntityInterface;

  /**
   * Gets the trigger entity that fired the event.
   *
   * @return \Drupal\commerce_eta\Entity\TriggerInterface
   *   The trigger entity.
   */
  public function getTrigger() : TriggerInterface;

  /**
   * Gets the trigger entity id that fired the event.
   *
   * @return string
   *   The Trigger config entity id.
   */
  public function getTriggerId() : string;

}
