<?php

namespace Drupal\commerce_eta\Entity;

use Drupal\commerce\ConditionGroup;
use Drupal\commerce\Plugin\Commerce\Condition\ParentEntityAwareInterface;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityInterface;

/**
 * Defines the Trigger entity.
 *
 * @ConfigEntityType(
 *   id = "commerce_event_trigger",
 *   label = @Translation("Trigger"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\commerce_eta\TriggerListBuilder",
 *     "form" = {
 *       "add" = "Drupal\commerce_eta\Form\TriggerForm",
 *       "edit" = "Drupal\commerce_eta\Form\TriggerForm",
 *       "delete" = "Drupal\commerce_eta\Form\TriggerDeleteForm",
 *       "purge" = "Drupal\commerce_eta\Form\TriggerEventPurgeForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\commerce_eta\TriggerHtmlRouteProvider",
 *     },
 *   },
 *   bundle_of = "commerce_event_log_item",
 *   config_prefix = "commerce_event_trigger",
 *   admin_permission = "administer commerce event triggers",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "status" = "status",
 *     "cron_status" = "cron_status",
 *     "log_status" = "log_status",
 *     "run_limit" = "run_limit",
 *     "target_entity_type" = "target_entity_type",
 *     "conditions",
 *     "conditionOperator",
 *   },
 *   config_export = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "status" = "status",
 *     "cron_status" = "cron_status",
 *     "log_status" = "log_status",
 *     "run_limit" = "run_limit",
 *     "target_entity_type" = "target_entity_type",
 *     "conditions",
 *     "conditionOperator",
 *   },
 *   links = {
 *     "canonical" = "/admin/commerce/config/events/trigger/{commerce_event_trigger}",
 *     "add-form" = "/admin/commerce/config/events/trigger/add",
 *     "edit-form" = "/admin/commerce/config/events/trigger/{commerce_event_trigger}/edit",
 *     "purge-form" = "/admin/commerce/config/events/trigger/{commerce_event_trigger}/purge",
 *     "delete-form" = "/admin/commerce/config/events/trigger/{commerce_event_trigger}/delete",
 *     "collection" = "/admin/commerce/config/events/triggers"
 *   }
 * )
 */
class Trigger extends ConfigEntityBase implements TriggerInterface {

  /**
   * The Trigger ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Trigger label.
   *
   * @var string
   */
  protected $label;

  /**
   * The enabled status of the trigger.
   *
   * @var bool
   */
  protected $status = FALSE;

  /**
   * The status of cron for this trigger.
   *
   * @var bool
   */
  protected $cron_status;

  /**
   * The status of logging for this trigger.
   *
   * @var bool
   */
  protected $log_status;

  /**
   * The amount of times this trigger can run on any one entity.
   *
   * @var int|null
   */
  protected $run_limit = NULL;

  /**
   * The Commerce entity this trigger fires on.
   *
   * @var string
   */
  protected $target_entity_type;

  /**
   * The conditions.
   *
   * @var array
   */
  protected $conditions = [];

  /**
   * The condition operator.
   *
   * @var string
   */
  protected $conditionOperator = 'AND';

  /**
   * {@inheritdoc}
   */
  public function getEventName(): string {
    return "commerce_eta." . $this->id();
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus(): bool {
    return $this->status;
  }

  /**
   * {@inheritdoc}
   */
  public function getCronStatus() : bool {
    return $this->cron_status;
  }

  /**
   * {@inheritdoc}
   */
  public function getLogStatus() : bool {
    return $this->log_status;
  }

  /**
   * {@inheritdoc}
   */
  public function getRunLimit() : ?int {
    return $this->run_limit;
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetEntityTypeId() : string {
    return $this->target_entity_type;
  }

  /**
   * {@inheritdoc}
   */
  public function getConditions() : array {
    $plugin_manager = \Drupal::service('plugin.manager.commerce_condition');
    $conditions = [];
    foreach ($this->conditions as $condition) {
      $condition = $plugin_manager->createInstance($condition['plugin'], $condition['configuration']);
      if ($condition instanceof ParentEntityAwareInterface) {
        $condition->setParentEntity($this);
      }
      $conditions[] = $condition;
    }
    return $conditions;
  }

  /**
   * {@inheritdoc}
   */
  public function getConditionOperator() : string {
    return $this->conditionOperator;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(EntityInterface $entity) : bool {
    $conditions = $this->getConditions();
    if (!$conditions) {
      // Payment gateways without conditions always apply.
      return TRUE;
    }
    $order_conditions = array_filter($conditions, function ($condition) {
      /** @var \Drupal\commerce\Plugin\Commerce\Condition\ConditionInterface $condition */
      return $condition->getEntityTypeId() == 'commerce_order';
    });
    $order_conditions = new ConditionGroup($order_conditions, $this->getConditionOperator());

    return $order_conditions->evaluate($entity);
  }

}
