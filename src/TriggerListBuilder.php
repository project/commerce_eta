<?php

namespace Drupal\commerce_eta;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a listing of Trigger entities.
 */
class TriggerListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('entity_type.manager')->getStorage('commerce_event_log_item'),
    );
  }

  /**
   * Constructs a new EntityListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\commerce_eta\EventLogItemStorage $eventLogItemStorage
   *   Event log item entity storage.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, protected EventLogItemStorage $eventLogItemStorage) {
    $this->entityTypeId = $entity_type->id();
    $this->storage = $storage;
    $this->entityType = $entity_type;
  }

  /**
   * Event Log Item Storage.
   *
   * @var \Drupal\commerce_eta\EventLogItemStorage
   */
  protected $storage;

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Trigger');
    $header['event_name'] = $this->t('Event name');
    $header['target_entity_type'] = $this->t('Target Entity Type');
    $header['cron_status'] = $this->t('Cron');
    $header['status'] = $this->t('Status');
    $header['events'] = $this->t("Events");
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\commerce_eta\Entity\Trigger $entity */
    $row['label'] = $entity->label();
    $row['event_name'] = $entity->getEventName();
    $row['target_entity_type'] = \Drupal::entityTypeManager()->getDefinition($entity->getTargetEntityTypeId())->getLabel();
    $row['cron_status'] = $entity->getCronStatus() ? $this->t("Enabled") : $this->t("Disabled");
    $row['status'] = $entity->getStatus() ? $this->t("Enabled") : $this->t("Disabled");
    $row['events'] = $this->eventLogItemStorage->getCount($entity);
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    /** @var \Drupal\Core\Config\Entity\ConfigEntityInterface $entity */
    $operations = parent::getDefaultOperations($entity);

    if ($entity->hasLinkTemplate('purge-form')) {
      $operations['enable'] = [
        'title' => $this->t('Purge Events'),
        'weight' => -10,
        'url' => $this->ensureDestination($entity->toUrl('purge-form')),
      ];
    }
    return $operations;
  }

}
