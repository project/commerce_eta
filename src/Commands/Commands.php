<?php

namespace Drupal\commerce_eta\Commands;

use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drupal\commerce_eta\Entity\TriggerInterface;
use Drupal\commerce_eta\TriggerDispatch;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drush\Commands\DrushCommands;

/**
 * Custom Drush command file for managing event triggers via CLI.
 */
class Commands extends DrushCommands {

  /**
   * Trigger Dispatch Service.
   *
   * @var \Drupal\commerce_eta\TriggerDispatch
   */
  protected $dispatch;

  /**
   * Entity Type Manager Service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs the Drush command class.
   *
   * @param \Drupal\commerce_eta\TriggerDispatch $trigger_dispatch
   *   Trigger dispatch service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity Type Manager Service.
   */
  public function __construct(TriggerDispatch $trigger_dispatch, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct();
    $this->dispatch = $trigger_dispatch;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Fire all enabled triggers.
   *
   * @param array $options
   *   Array of cli switch options.
   *
   * @usage commerce_eta:triggers:fire:all
   *   Fire all triggers.
   *
   * @command commerce_eta:triggers:fire:all
   */
  public function triggersFireAll(array $options = ["dry" => FALSE]) {
    /** @var \Drupal\commerce_eta\Entity\TriggerInterface[] $triggers */
    $triggers = $this->entityTypeManager->getStorage('commerce_event_trigger')->loadMultiple();
    if ($options['dry']) {
      $this->writeln(dt("Dry firing @count triggers...", [
        '@count' => count($triggers),
      ]));
    }
    else {
      $this->writeln(dt("Firing @count triggers...", [
        '@count' => count($triggers),
      ]));
    }
    foreach ($triggers as $trigger) {
      if (!$trigger->getStatus()) {
        $this->writeln(dt("@label is disabled, no events will be fired.", [
          '@label' => $trigger->label(),
        ]));
        continue;
      }

      // Fire the trigger.
      $this->writeln(dt("@firing @label...", [
        '@firing' => $options['dry'] ? "Dry firing" : "Firing",
        '@label' => $trigger->label(),
      ]));

      $event_count = $this->dispatch->fire($trigger, $options['dry']);

      $this->writeln(dt("@count events fired on @label", [
        '@count' => $event_count,
        '@label' => $trigger->label(),
      ]));
    }
  }

  /**
   * Command description here.
   *
   * @param string $trigger_id
   *   Trigger entity id.
   * @param array $options
   *   Array of cli switch options.
   *
   * @usage commerce_eta:triggers:fire abandoned_carts
   *   Requires a trigger id to fire one trigger.
   *
   * @command commerce_eta:triggers:fire
   */
  public function triggersFireOne(string $trigger_id, array $options = ["dry" => FALSE]) {
    $trigger = $this->entityTypeManager->getStorage('commerce_event_trigger')->load($trigger_id);

    // Fail out if trigger id is not valid.
    if (!$trigger instanceof TriggerInterface) {
      $this->writeln(dt("@trigger_id is not a valid trigger", [
        '@trigger_id' => $trigger_id,
      ]));
      return;
    }

    // Fail out early if trigger is disabled.
    if (!$trigger->getStatus()) {
      $this->writeln(dt("@label is disabled, no events will be fired.", [
        '@label' => $trigger->label(),
      ]));
      return;
    }

    // Fire the trigger.
    $this->writeln(dt("@firing @label...", [
      '@firing' => $options['dry'] ? "Dry firing" : "Firing",
      '@label' => $trigger->label(),
    ]));

    $event_count = $this->dispatch->fire($trigger, $options['dry']);

    $this->writeln(dt("@count events fired on @label", [
      '@count' => $event_count,
      '@label' => $trigger->label(),
    ]));
  }

  /**
   * List all triggers available to be fired.
   *
   * @field-labels
   *   trigger_id: Trigger Id
   *   event_name: Event Name
   *   target_entity: Target Entity
   *   cron_status: Cron Status
   *   next_run: Next Run
   *   status: Status
   *
   * @usage commerce_eta:triggers:list
   *   List all triggers.
   *
   * @default-fields trigger_id,event_name,target_entity,cron_status,next_run,status
   *
   * @command commerce_eta:triggers:list
   *
   * @filter-default-field trigger
   *
   * @return \Consolidation\OutputFormatters\StructuredData\RowsOfFields
   *   Returns an array of table rows.
   */
  public function listTriggers($options = ['format' => 'table']) {
    /** @var \Drupal\commerce_eta\Entity\TriggerInterface[] $triggers */
    $triggers = $this->entityTypeManager->getStorage('commerce_event_trigger')->loadMultiple();

    $rows = [];

    foreach ($triggers as $trigger) {

      $rows[] = [
        'trigger_id' => $trigger->id(),
        'event_name' => $trigger->getEventName(),
        'target_entity' => $this->entityTypeManager->getDefinition($trigger->getTargetEntityTypeId())->getLabel(),
        'cron_status' => $trigger->getCronStatus() ? dt("Enabled") : dt('Disabled'),
        'status' => $trigger->getStatus() ? dt('Enabled') : dt('Disabled'),
      ];
    }

    return new RowsOfFields($rows);
  }

}
